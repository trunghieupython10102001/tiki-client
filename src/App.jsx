import Cookies from 'js-cookie';
import { useEffect } from 'react';
import { useRoutes } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import router from './routes/router';
import { configAuthHeader } from './api/index';
import { KEYS } from './constants/authState';
import { autoLogin } from './store/user';

import './App.css';

function App() {
    const routes = useRoutes(router);
    const dispatch = useDispatch();
    useEffect(() => {
        const token = Cookies.get(KEYS.TOKEN);
        if (token) {
            configAuthHeader(token);
            dispatch(autoLogin(token));
        }
    }, []);

    return <div className="App">{routes}</div>;
}

export default App;
