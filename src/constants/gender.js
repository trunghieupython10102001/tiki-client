const GENDER_KEYS = {
    male: 1,
    female: 2,
    other: 3,
}

export default {
    [GENDER_KEYS.male]: 'Nam',
    [GENDER_KEYS.female]: 'Nữ',
    [GENDER_KEYS.other]: 'Khác',
};
