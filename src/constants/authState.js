export const KEYS = {
    TOKEN: "token"
}

export default {
    enteredPhone: "enteredPhone",
    forgotPass: "forgotPass",
    getPassword: "getPassword"
}