import { Card, Col, Divider, Row, Modal, Button, message } from 'antd';
import { useParams, useNavigate } from 'react-router-dom';
import orderStates, { STATE } from '../constants/orderState';
import paymentMethods from '../constants/paymentMethods';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import moment from 'moment';
import Order from '../components/OrderDetail/Order';
import api from '../api';
import { useEffect, useState } from 'react';

function OrderDetail() {
    const { orderID } = useParams();
    const navigator = useNavigate();
    const [order, setOrder] = useState({});

    function goBack() {
        navigator(-1);
    }

    function cancelOrder() {
        const modal = Modal.confirm({
            title: 'Bạn có chắc chắn muốn hủy đơn hàng (Bạn sẽ không thể hoàn tác hành động này)?',
            cancelText: 'Quay lại',
            okText: 'Hủy',
            onOk: () => {
                api.order
                    .update(orderID, { status: STATE.CANCEL })
                    .then(() => {
                        setOrder({ ...order, state: STATE.CANCEL });
                    })
                    .catch(() => {
                        message.error('Có lỗi xảy ra, thử lại sau');
                    });
                modal.destroy();
            },
            onCancel: () => {
                modal.destroy();
            },
        });
    }

    useEffect(() => {
        (async () => {
            const { data: resOrder } = await api.order.getOrder(orderID);
            const orderInfo = {
                deliveryAddress: resOrder.address,
                items: resOrder.order_items.map((item) => ({
                    amount: item.amount,
                    id: item.product_id,
                    name: item.product.name,
                    price: item.product.price,
                    img: item.product.thumbnail_url,
                })),
                note: resOrder.note,
                paymentMethod: resOrder.payment_method,
                customerPhone: resOrder.phone_number,
                state: resOrder.status.toUpperCase(),
                totalPrice: resOrder.amount,
                customerName: resOrder.recipient_name,
                id: resOrder.id,
                createTime: resOrder.order_date,
            };

            setOrder(orderInfo);
        })();
    }, [orderID]);
    return (
        <Card
            title={
                <div className="flex justify-between items-center">
                    <button
                        className="text-lg cursor-pointer uppercase hover:text-blue-400"
                        onClick={goBack}
                    >
                        <FontAwesomeIcon icon={['fas', 'chevron-left']} /> Trở lại
                    </button>
                    <div className="flex text-lg">
                        <h4>Mã đơn hàng: {orderID}</h4>
                        <Divider
                            type="vertical"
                            className="!border-l-gray-600 !mx-4"
                            style={{ height: 'auto' }}
                        />
                        <p className="m-0 uppercase text-green-400">{orderStates[order.state]}</p>
                    </div>
                </div>
            }
            bodyStyle={{
                fontSize: '1rem',
            }}
        >
            <Row gutter={[16, 8]}>
                <Col span={12}>
                    <strong>Họ tên:</strong> {order.customerName}
                </Col>
                <Col span={12}>
                    <strong>Số điện thoại:</strong> {order.customerPhone}
                </Col>
                <Col span={12}>
                    <strong>Ngày đặt:</strong> {moment(order.createTime).format('DD/MM/YYYY')}
                </Col>
                <Col span={24}>
                    <strong>Địa chỉ:</strong> {order.deliveryAddress}
                </Col>
                <Col span={24}>
                    <strong>Ghi chú:</strong> {order.note || 'Không'}
                </Col>
            </Row>
            <Divider />
            <ul className="ordered__list">
                {order.items?.map((item) => (
                    <Order item={item} key={item.id} />
                ))}
            </ul>
            <div className="mt-5 flex justify-between items-start">
                {order.state === STATE.PROCESSING && (
                    <Button onClick={cancelOrder} type="primary" danger>
                        Hủy đơn hàng
                    </Button>
                )}
                <table className="ml-auto">
                    <tbody>
                        <tr>
                            <td>
                                <span className="mr-4">Tổng tiền:</span>
                            </td>
                            <td className="text-lg font-bold text-red-500">
                                {order.totalPrice?.toLocaleString()} VNĐ
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span className="mr-4"> Phương thức thanh toán:</span>
                            </td>
                            <td>{paymentMethods[order.paymentMethod]}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </Card>
    );
}

export default OrderDetail;
