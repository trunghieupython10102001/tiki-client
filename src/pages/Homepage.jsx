import { Space } from "antd";
import { useEffect, useState } from "react";
import Banner from "../components/Homepage/Banner";
import FlashDeal from "../components/Homepage/FlashDeal/FlashDeal";
import HotCategories from "../components/Homepage/HotCategories/HotCategories";
import ProductItems from "../components/Homepage/ProductItems/ProductItems";
import TabContainer from "../components/Homepage/sliders/TabContainer";
import api from '../api/index';

// import {  flashDealItems } from "../constants/fake-data";

function HomePage() {
    const [topCategories, setTopCategories] = useState([])
    const [flashDealItems, setFlashDealItems] = useState([])
    const [categories, setCategories] = useState([])
    const [suggestProducts, setSuggestProducts] = useState([])
    const [topProducts, setTopProducts] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const [topCategoriesRes, categoriesRes, flashDealRes, suggestProductsRes, topProductsRes] = await Promise.all([
                api.category.getAll(),
                api.category.getAll({ limit: 30 }),
                api.product.getAll({ sort_by: 'sale_percent', order_by: 'desc', limit: 6 }),
                api.product.getAll({ random: 1, limit: 36 }),
                api.product.getAll({ order_by: 'desc', limit: 6 }),
            ]);

            setTopCategories(topCategoriesRes.data);
            setFlashDealItems(flashDealRes.data);
            setCategories(categoriesRes.data);
            setSuggestProducts(suggestProductsRes.data);
            setTopProducts(topProductsRes.data);
        }

        fetchData();
    }, [])
    

    return (
        <div>
            <TabContainer tabList={topCategories} />
            <div className="mx-5 my-4 pb-4">
                <Space direction="vertical" size="middle" style={{ display: "flex" }}>
                    <Banner topProducts={topProducts} />
                    <FlashDeal dealItems={flashDealItems} />
                    <HotCategories hotCategories={categories} />
                    <ProductItems products={suggestProducts} />
                </Space>
            </div>
        </div>
    );
}

export default HomePage;
