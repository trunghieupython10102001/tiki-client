import { useEffect, useState, useRef } from 'react';
import { useParams, useSearchParams } from 'react-router-dom';
import { products as productList } from '../constants/fake-data';
import ProductOverview from '../components/ProductDetail/ProductOverview';
import ProductDetailContent from '../components/ProductDetail/ProductDetailContent';
import RecommendProducts from '../components/ProductDetail/RecommendProducts';
import ProductComment from '../components/ProductDetail/ProductComment';
import LoadingPlaceholder from '../components/shared/LoadingPlaceholder';
import { useDispatch, useSelector } from 'react-redux';
import { Breadcrumb, message, Modal } from 'antd';
import { checkoutActions } from '../store/cart';
import FeedbackForm from '../components/ProductDetail/FeedbackForm';
import api from '../api';

const DEFAULT_COMMENT_LIMIT = 5;

let isFirst = false;

function ProductDetail() {
    const { productId } = useParams();
    const [query] = useSearchParams();
    const [isLoading, setIsLoading] = useState(true);
    const [isSubmittingRating, setIsSubmittingRating] = useState(false);
    const [showFeedback, setShowFeedback] = useState(false);
    const [product, setProduct] = useState(null);
    const [recommendProducts, setRecommendProducts] = useState([]);
    const [commentList, setCommentList] = useState({});
    const commentRef = useRef();
    const uid = useSelector((store) => store.user.id);
    const dispatch = useDispatch();
    const commentPage = query.get('page') || '1';

    async function addProductToCart(orderCount) {
        try {
            await api.cart.updateCart({
                product_id: productId,
                user_id: uid,
                amount: orderCount,
            });

            const cart = {
                product: { ...product, img: product.thumbnail_url },
                quantity: orderCount,
            };
            dispatch(checkoutActions.addItemToCart(cart));
            message.success('Thêm sản phẩm thành công');
        } catch (error) {
            console.log(error);
            message.error('Không thể cập nhật sản phẩm');
        }
    }

    function scrollToComments() {
        commentRef.current.scrollIntoView();
    }

    function showFeedbackForm() {
        setShowFeedback(true);
    }

    function hideFeedbackForm() {
        setShowFeedback(false);
    }

    async function loadData() {
        try {
            const [{ data: productInfo }, { data: ratings }] = await Promise.all([
                api.product.getOne(productId, { user_id: uid }),
                api.product.getRatings({ product_id: productId, order_by: 'desc' }),
            ]);

            productInfo.specifications = JSON.parse(productInfo.specifications);
            const { rating_1, rating_2, rating_3, rating_4, rating_5, ratings_count, avg_ratings } =
                productInfo;
            const ratingRatio = {
                1: { count: rating_1, percent: (rating_1 / ratings_count) * 100 },
                2: { count: rating_2, percent: (rating_2 / ratings_count) * 100 },
                3: { count: rating_3, percent: (rating_3 / ratings_count) * 100 },
                4: { count: rating_4, percent: (rating_4 / ratings_count) * 100 },
                5: { count: rating_5, percent: (rating_5 / ratings_count) * 100 },
            };
            const ratingData = ratings.map(({ user, comment, id, rating }) => ({
                created_by: user,
                id,
                rating,
                content: comment,
            }));
            const comments = {
                avg_ratings,
                stars: ratingRatio,
                ratings_count,
                data: ratingData,
                paging: {
                    total: ratings_count,
                    per_page: DEFAULT_COMMENT_LIMIT,
                    current_page: commentPage,
                },
            };

            document.head.querySelector('title').textContent = productInfo.name;
            setProduct(productInfo);
            setCommentList(comments);
        } catch (error) {
            console.log(error);
            message.error('Có lỗi xảy ra, vui lòng vào lại sau');
        }
    }

    async function submitFeedback(form) {
        setIsSubmittingRating(true);
        try {
            await api.product.addRating({
                ...form,
                product_id: productId,
                user_id: uid,
            });

            message.success('Thêm đánh giá thành công');
            await loadData();
            hideFeedbackForm();
        } catch (error) {
            console.log(error);
            message.error('Có lỗi xảy ra, thử lại sau');
        } finally {
            setIsSubmittingRating(false);
        }
    }

    const breadcrumbs = [
        {
            url: '/',
            label: 'Trang chủ',
        },
        {
            url: '/san-pham',
            label: 'Sản phẩm',
        },
        {
            label: product?.name,
        },
    ];

    useEffect(() => {
        setIsLoading(true);
        window.scrollTo({ top: 0, behavior: 'smooth' });
        isFirst = false;

        (async () => {
            try {
                const [{ data: productInfo }, { data: ratings }] = await Promise.all([
                    api.product.getOne(productId, { user_id: uid }),
                    api.product.getRatings({
                        product_id: productId,
                        order_by: 'desc',
                        limit: DEFAULT_COMMENT_LIMIT,
                    }),
                ]);
                const { category_id } = productInfo;
                const { data: reProds } = await api.product.getAll({
                    category_id,
                    random: 1,
                    limit: 12,
                });

                productInfo.specifications = JSON.parse(productInfo.specifications);
                productInfo.avg_ratings = productInfo.avg_ratings.toPrecision(2);

                const {
                    rating_1,
                    rating_2,
                    rating_3,
                    rating_4,
                    rating_5,
                    ratings_count,
                    avg_ratings,
                } = productInfo;
                const ratingRatio = {
                    1: { count: rating_1, percent: (rating_1 / ratings_count) * 100 },
                    2: { count: rating_2, percent: (rating_2 / ratings_count) * 100 },
                    3: { count: rating_3, percent: (rating_3 / ratings_count) * 100 },
                    4: { count: rating_4, percent: (rating_4 / ratings_count) * 100 },
                    5: { count: rating_5, percent: (rating_5 / ratings_count) * 100 },
                };
                const ratingData = ratings.map(({ user, comment, id, rating }) => ({
                    created_by: user,
                    id,
                    rating,
                    content: comment,
                }));
                const comments = {
                    avg_ratings,
                    stars: ratingRatio,
                    ratings_count,
                    data: ratingData,
                    paging: {
                        total: ratings_count,
                        per_page: DEFAULT_COMMENT_LIMIT,
                        current_page: commentPage,
                    },
                };

                document.head.querySelector('title').textContent = productInfo.name;
                setProduct(productInfo);
                setCommentList(comments);
                setRecommendProducts(reProds);
            } catch (error) {
                console.log(error);
                message.error('Có lỗi xảy ra, vui lòng vào lại sau');
            } finally {
                setIsLoading(false);
            }
        })();
    }, [productId, uid]);

    useEffect(() => {
        (async () => {
            if (isFirst) {
                return;
            }

            try {
                const { data: ratings } = await api.product.getRatings({
                    product_id: productId,
                    order_by: 'desc',
                    page: commentPage,
                    limit: DEFAULT_COMMENT_LIMIT,
                });

                const ratingData = ratings.map(({ user, comment, id, rating }) => ({
                    created_by: user,
                    id,
                    rating,
                    content: comment,
                }));
                const comments = {
                    ...commentList,
                    data: ratingData,
                    paging: {
                        total: commentList.paging?.total || 0,
                        per_page: DEFAULT_COMMENT_LIMIT,
                        current_page: commentPage,
                    },
                };

                setCommentList(comments);
            } catch (error) {
                console.log(error);
                message.error('Có lỗi xảy ra, vui lòng vào lại sau');
            }
        })();
    }, [commentPage]);

    return (
        <section>
            <LoadingPlaceholder isLoading={isLoading}>
                <div className="px-10 py-4 bg-gray-100">
                    <Breadcrumb separator=">" className="!pb-4">
                        {breadcrumbs.map((link, i) => {
                            if (!link.url) {
                                return <Breadcrumb.Item key={i}>{link.label}</Breadcrumb.Item>;
                            }
                            return (
                                <Breadcrumb.Item key={i} href={link.url}>
                                    {link.label}
                                </Breadcrumb.Item>
                            );
                        })}
                    </Breadcrumb>
                    <ProductOverview
                        product={product}
                        onAddToCart={addProductToCart}
                        onScrollToComments={scrollToComments}
                    />
                    <ProductDetailContent product={product} className="my-5" />
                    <div className="my-4">
                        <ProductComment
                            comments={commentList}
                            ref={commentRef}
                            onFeedback={showFeedbackForm}
                            commentable={product?.commentable}
                        />
                    </div>
                    <RecommendProducts products={recommendProducts} />
                    <Modal visible={showFeedback} footer={null} onCancel={hideFeedbackForm}>
                        <FeedbackForm onSubmit={submitFeedback} isSubmitting={isSubmittingRating} />
                    </Modal>
                </div>
            </LoadingPlaceholder>
        </section>
    );
}

export default ProductDetail;
