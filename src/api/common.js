import {configedAxios as axios} from "./index";

export default {
    upload: (data) => axios.post('/api/upload', data).then((_) => _.data),
};
