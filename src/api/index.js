import axios from 'axios';
import Cookies from 'js-cookie';
import auth from './auth';
import user from './user';
import common from './common';
import product from './product';
import category from './category';
import order from './order';
import cart from './cart';
import { BASE_URL } from '../constants/endPoint';

export const configedAxios = axios.create({
    baseURL: BASE_URL,
    withCredentials: true,
});

function requestHandler(config) {
    if (
        (config.method == 'post' || config.method == 'put' || config.method == 'delete') &&
        !Cookies.get('XSRF-TOKEN')
    ) {
        return setCSRFToken().then(() => config);
    }
    return config;
}

function setCSRFToken() {
    return configedAxios.get('/sanctum/csrf-cookie');
}

function responseHandler(response) {
    if (response.data.status && response.data.status >= 400) {
        return Promise.reject(response.data);
    }
    return response;
}

configedAxios.interceptors.request.use(requestHandler, null);
configedAxios.interceptors.response.use(responseHandler, null);

export function configAuthHeader(token) {
    configedAxios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}

export default {
    auth,
    user,
    common,
    product,
    category,
    order,
    cart,
};
