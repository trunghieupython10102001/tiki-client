import { configedAxios as axios } from './index';

const URL = 'products';

export default {
    getAll: (params) => axios.get(`/api/${URL}`, { params }).then((_) => _.data),
    getOne: (id, params) => axios.get(`/api/${URL}/${id}`, { params }).then((_) => _.data),
    getRatings: (params) => axios.get(`/api/ratings`, { params }).then((_) => _.data),
    addRating: (form) => axios.post(`/api/ratings`, form).then((_) => _.data),
};
