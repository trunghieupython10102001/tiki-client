import { configedAxios as axios } from './index';

const URL = "categories";

export default {
    getAll: (params) => axios.get(`/api/${URL}`, { params }).then((_) => _.data),
    getOne: (id, params) => axios.get(`/api/${URL}/${id}`, { params }).then((_) => _.data),
};
