import { configedAxios as axios } from './index';

export default {
    getInfo: () => axios.get('/api/me').then((_) => _.data),
    updateUser: (id, user) => axios.put('/api/user/update/' + id, user).then((_) => _.data),
};
