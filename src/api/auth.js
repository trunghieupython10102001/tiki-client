import { configedAxios as axios, configAuthHeader } from './index';
import { KEYS } from '../constants/authState';
import Cookies from 'js-cookie';

export default {
    signIn: (form) =>
        axios
            .post('/api/auth/login', form)
            .then((_) => _.data)
            .then((authData) => {
                configAuthHeader(authData.accessToken);
                Cookies.set(KEYS.TOKEN, authData.accessToken, { expires: 7 });
                return authData;
            }),

    logout: () =>
        axios
            .post('/api/auth/logout')
            .then((_) => _.data)
            .then(() => {
                Cookies.remove(KEYS.TOKEN);
            }),
    forgetPassword: (data) => axios.post('/api/auth/forgotPassword', data).then((_) => _.data),
    changePassword: (data) => axios.post('/api/auth/change-password', data).then((_) => _.data),
};
