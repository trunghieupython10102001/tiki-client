import { configedAxios as axios } from './index';

export default {
    create: (data) => axios.post('/api/orders', data).then((_) => _.data),
    update: (id, form) => axios.put(`/api/orders/${id}`, form).then((_) => _.data),
    getAll: (params) => axios.get('/api/orders', { params }).then((_) => _.data),
    getOrder: (id) => axios.get(`/api/orders/${id}`).then((_) => _.data),
};
