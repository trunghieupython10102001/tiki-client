import { configedAxios as axios } from './index';

export default {
    getCart: (user_id) => axios.get('/api/carts', { params: { user_id } }).then((_) => _.data),
    getCartCount: (user_id) =>
        axios.get('/api/carts/count', { params: { user_id } }).then((_) => _.data),
    updateCart: (form) => axios.post('/api/carts', form).then((_) => _.data),
};
