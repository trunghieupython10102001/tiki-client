import PropTypes from "prop-types";
import { Link } from "react-router-dom";

function SliderTab({ name, id }) {
    return (
        <li className="px-4 py-2 flex-auto text-center hover:bg-white">
            <Link to={`/danh-muc/${id}`} className="text-black hover:text-blue-500">
                {name}
            </Link>
        </li>
    );
}

SliderTab.propTypes = {
    name: PropTypes.string,
    id: PropTypes.string,
};

export default SliderTab;
