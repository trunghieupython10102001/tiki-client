import { Button, Form, Input, Rate } from 'antd';
import { useState } from 'react';
import PropTypes from 'prop-types';

const defaultForm = {
    rating: undefined,
    comment: '',
};

function FeedbackForm({ onSubmit, isSubmitting }) {
    const RatingLabels = ['Rất tệ', 'Tệ', 'Bình thường', 'Tốt', 'Rất tốt'];
    const [form, setForm] = useState({ ...defaultForm });

    function ratingProduct(rating) {
        setForm({ ...form, rating });
    }

    function commentHandler(e) {
        setForm({ ...form, comment: e.target.value });
    }

    function submitForm() {
        onSubmit(form);
    }

    return (
        <div>
            <Form layout="vertical">
                <Form.Item label="Đánh giá">
                    <span>
                        <Rate
                            tooltips={RatingLabels}
                            onChange={ratingProduct}
                            value={form.rating}
                        />
                        {form.rating ? (
                            <span className="ant-rate-text">{RatingLabels[form.rating - 1]}</span>
                        ) : (
                            ''
                        )}
                    </span>
                </Form.Item>
                <Form.Item label="Chia sẻ cảm nhận của bạn">
                    <Input.TextArea
                        autoSize={{ minRows: 5, maxRows: 7 }}
                        value={form.comment}
                        onChange={commentHandler}
                    />
                </Form.Item>
                <Button
                    type="primary"
                    onClick={submitForm}
                    loading={isSubmitting}
                    disabled={isSubmitting}
                >
                    Gửi
                </Button>
            </Form>
        </div>
    );
}

FeedbackForm.propTypes = {
    onSubmit: PropTypes.string,
    isSubmitting: PropTypes.bool,
};

export default FeedbackForm;
