import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import ratingLabel from '../../constants/rating';
import './ProductCommentItem.scss';

function ProductCommentItem({ comment, className = '' }) {
    return (
        <li className={'flex items-start ' + className}>
            <img
                src={comment.created_by.avatar_url}
                alt=""
                className="w-12 h-12 rounded-full mr-5"
            />
            <div>
                <p className="mb-2">{comment.created_by.username}</p>
                <p>
                    {Array.apply(null, new Array(5)).map((_, i) => {
                        if (i + 1 <= comment.rating) {
                            return (
                                <FontAwesomeIcon
                                    icon={['fas', 'star']}
                                    className="text-yellow-400"
                                    key={i}
                                />
                            );
                        }
                        return (
                            <FontAwesomeIcon
                                icon={['fas', 'star']}
                                className="text-gray-400"
                                key={i}
                            />
                        );
                    })}
                    <span className="ml-2 font-bold">{ratingLabel[comment.rating]}</span>
                </p>
                <p>{comment.content}</p>
            </div>
        </li>
    );
}

ProductCommentItem.propTypes = {
    comments: PropTypes.object,
    className: PropTypes.string,
    onFavourite: PropTypes.func,
};

export default ProductCommentItem;
