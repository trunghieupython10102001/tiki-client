import { useCallback, useState } from 'react';
import _debounce from 'lodash/debounce';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { InputNumber, Table } from 'antd';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import './ItemList.scss';

function ItemList({ onUpdateItem, onRemoveItem, isUpdating }) {
    const cartItems = useSelector((store) => store.cart.cartItems);
    const items = cartItems.map((item) => ({ ...item, key: item.id }));
    const [itemsAmount, setItemsAmount] = useState(
        cartItems.reduce((amountObj, item) => {
            amountObj[item.id] = item.amount;
            return amountObj;
        }, {})
    );

    const updateItemAmount = useCallback(
        _debounce(function (itemID, amount) {
            onUpdateItem({ itemID, amount: Number(amount) });
        }, 1000),
        []
    );

    function updateItem(itemID, amount) {
        console.log(itemID);
        console.log(amount);
        setItemsAmount((amounts) => ({ ...amounts, [itemID]: Number(amount) }));
        updateItemAmount(itemID, amount);
        if (amount === 0) {
            removeCartItem(itemID);
            return;
        }
    }

    function removeCartItem(itemID) {
        onRemoveItem(itemID);
    }

    return (
        <Table
            rowClassName="w-full"
            dataSource={items}
            pagination={{ style: { display: 'none' } }}
            onHeaderRow={() => ({
                className: 'item-list__table-header-row',
            })}
            columns={[
                {
                    title: 'Sản phẩm',
                    key: 'item',
                    render: (_, item) => {
                        return (
                            <Link
                                to={`/san-pham/${item.id}`}
                                className="flex items-center"
                                style={{
                                    minWidth: '300px',
                                }}
                            >
                                <img src={item.img || item.thumbnail_url} alt="" className="w-20 h-20 object-cover" />
                                <p className="m-0 ml-4 text-black item-list__item-name hover:text-blue-500">
                                    {item.name}
                                </p>
                            </Link>
                        );
                    },
                },
                {
                    title: 'Đơn giá',
                    key: 'price',
                    dataIndex: 'price',
                    render: (price) => <strong>{price.toLocaleString()} VNĐ</strong>,
                    width: 150,
                },
                {
                    title: 'Số lượng',
                    key: 'amount',
                    dataIndex: 'amount',
                    width: 120,
                    render: (_, item) => (
                        <InputNumber
                            value={itemsAmount[item.id]}
                            controls={false}
                            className="item-list__amount"
                            disabled={isUpdating}
                            addonBefore={
                                <button
                                    className="block w-full"
                                    onClick={() => {
                                        updateItem(item.id, itemsAmount[item.id] - 1);
                                    }}
                                    disabled={isUpdating}
                                >
                                    -
                                </button>
                            }
                            addonAfter={
                                <button
                                    className="block w-full"
                                    onClick={() => {
                                        updateItem(item.id, itemsAmount[item.id] + 1);
                                    }}
                                    disabled={isUpdating}
                                >
                                    +
                                </button>
                            }
                            onChange={(value) => updateItem(item.id, value)}
                        />
                    ),
                },
                {
                    title: 'Thành tiền',
                    key: 'totalPrice',
                    dataIndex: 'totalPrice',
                    width: 200,
                    render: (total) => (
                        <strong className="text-red-500">{total.toLocaleString()} VNĐ</strong>
                    ),
                },
                {
                    key: 'action',
                    width: 50,
                    render: (_, item) => (
                        <button
                            onClick={() => {
                                removeCartItem(item.id);
                            }}
                        >
                            <FontAwesomeIcon icon={['fas', 'trash']} />
                        </button>
                    ),
                },
            ]}
        ></Table>
    );
}

ItemList.propTypes = {
    onUpdateItem: PropTypes.func,
    onRemoveItem: PropTypes.func,
    isUpdating: PropTypes.bool,
};

export default ItemList;
