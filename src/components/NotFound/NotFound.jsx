import { Link } from 'react-router-dom';
import NotFoundImg from '../../assets/img/404.png';
import './NotFound.scss';

const NotFound = () => {
    return (
        <section className="py-5">
            <div className="flex items-end w-4/5 mx-auto ">
                <figure>
                    <img className="w-50" src={NotFoundImg} alt="404 image" />
                </figure>
                <div className="flex items-end">
                    <h1 className="text-9xl  text-blue-500 m-0">404</h1>
                    <div className="ml-20 text-xl">
                        <p className="p-0">Xin lỗi, trang bạn đang tìm kiếm không tồn tại!</p>

                        <Link to="/" className="page_404__link_404">
                            Quay về trang chủ
                        </Link>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default NotFound;
