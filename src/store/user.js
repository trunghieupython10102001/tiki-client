import { createSlice } from '@reduxjs/toolkit';
import _cloneDeep from 'lodash/cloneDeep';
import api from '../api/index';
import { checkoutActions } from './cart';

const LOADING = 'LOADING';
const ERROR = 'ERROR';
const DONE = 'DONE';

export const STATE = {
    LOADING,
    ERROR,
    DONE,
};

const userInitialState = {
    auth: null,
    id: '',
    username: '',
    status: '',
    avatar: '',
    birthday: '',
    gender: '',
    address: '',
    isAdmin: null,
};

const userSlice = createSlice({
    name: 'user',
    initialState: _cloneDeep(userInitialState),
    reducers: {
        login(state, userData) {
            const { auth, user } = userData.payload;
            state.auth = auth;
            state.id = user.id;
            state.username = user.username || '';
            state.nickname = user.nick_name || '';
            state.avatar = user.avatar_url || 'https://salt.tikicdn.com/desktop/img/avatar.png';
            state.birthday = user.birthday || '';
            state.gender = String(user.gender);
            state.address = user.address || '';
            state.phone = user.phone_number;
            state.email = user.email || '';
            state.isAdmin = user.is_admin;
        },

        logOut() {
            return _cloneDeep(userInitialState);
        },

        setStatus(state, actions) {
            state.status = actions.payload;
        },

        updateUserInfo(state, { payload: userData }) {
            state.id = userData.id;
            state.username = userData.username;
            state.nickname = userData.nick_name;
            state.avatar = userData.avatar_url || 'https://salt.tikicdn.com/desktop/img/avatar.png';
            state.birthday = userData.birthday;
            state.gender = String(userData.gender);
            state.address = userData.address;
            state.phone = userData.phone_number;
            state.email = userData.email;
        },
    },
});

export const signIn = (form) => {
    return async (dispatch) => {
        try {
            dispatch(userSlice.actions.setStatus(LOADING));
            const authData = await api.auth.signIn(form);
            const user = await api.user.getInfo();
            const { data: cartAmount } = await api.cart.getCartCount(user.id);

            dispatch(checkoutActions.updateTotalAmount(cartAmount));
            dispatch(userSlice.actions.login({ auth: { token: authData.accessToken }, user }));
            dispatch(userSlice.actions.setStatus(DONE));
        } catch (error) {
            console.log(error);
            dispatch(userSlice.actions.setStatus(ERROR));
        }
    };
};

export const logout = () => {
    return async (dispatch) => {
        api.auth.logout();
        dispatch(userSlice.actions.logOut());
    };
};

export const autoLogin = (token) => {
    return async (dispatch) => {
        const userData = await api.user.getInfo();

        if (!userData) {
            return;
        } else {
            const { data } = await api.cart.getCart(userData.id);

            dispatch(checkoutActions.loadCart(data));
            dispatch(userSlice.actions.login({ auth: { token }, user: userData }));
        }
    };
};

export default userSlice;
export const userActions = userSlice.actions;
